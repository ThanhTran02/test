<!-- Banner -->
<p align="center">
  <a href="https://www.uit.edu.vn/" title="Trường Đại học Công nghệ Thông tin" style="border: none;">
    <img src="https://i.imgur.com/WmMnSRt.png" alt="Trường Đại học Công nghệ Thông tin | University of Information Technology">
  </a>
</p>

<h1 align="center"><b>PHÁT TRIÊN ỨNG DỤNG WEB</b></h1>

# Thành viên nhóm

| STT |   MSSV   |       Họ và Tên |    Chức Vụ |
| --- | :------: | --------------: | ---------: |
| 1   | 17520375 |   Ngọ Viết Dũng | Thành viên |
| 2   | 20521926 | Trần Ngọc Thành | Thành viên |
| 3   | 21522254 |   Lê Chánh Kiệt | Thành viên |
| 4   | 22521594 |       Vũ Anh Tú | Thành viên |
| 5   | 22521633 |  Phạm Văn tuyên | Thành viên |

# GIỚI THIỆU MÔN HỌC

- **Tên môn học:** Phát triển ứng dụng web
- **Mã môn học:** IS207.021
- **Năm học:** HK2 (2023 - 2024)
- **Giảng viên**:Nguyễn Dương Tùng

# ĐỒ ÁN CUỐI KÌ

- **Đề tài:** Trang web bán giày



# Project Setup

## 1. Clone Project

----->1.clone project
----->2. tạo file .env -> paste đoạn dưới -> chạy lệnh : npm i ->npm run dev


## 2. Set Up .env File

Create a `.env` 

```dotenv
NEXT_PUBLIC_BE_URL=http://localhost:8000/api/
GOOGLE_ID=
GOOGLE_SECRET=

```
